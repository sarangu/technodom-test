import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private apiURL = `${environment.apiUrl}`;
  private authHeader = {'Authorization': 'Bearer ' + localStorage.getItem('accessToken')};


  constructor(private httpClient: HttpClient) { }

  allProducts(){
    return this.httpClient.get(this.apiURL + 'list', {headers: this.authHeader});
  }
}
