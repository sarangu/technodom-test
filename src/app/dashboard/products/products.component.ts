import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductsService } from './products.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private router: Router, private ps: ProductsService, private spinner: NgxSpinnerService) { }
  private errmsg: string;
  private productsList = new Array();
  isDesc: boolean = false;
  column: string = 'name';

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.spinner.show();
    this.ps.allProducts().pipe().subscribe(res => {
      this.success(res);
    }, err => this.errhndlr(err));
  }

  success(data) {
    this.spinner.hide();
    if (data) {
      if (data.products) {
        data.products.map(elem => elem.subcategory.map(val => this.productsList.push(val)) );
        // data.products.forEach(element => {
        //   element.subcategory.forEach(val => {
        //     this.productsList.push(val);
        //   });
        // });
        // this.productsList = data.products;
        // console.log(this.productsList);
      } else {
        this.errmsg = 'No Records.'
      }
    } else {
      this.errmsg = 'No Records.'
    }
  }

  errhndlr(err) {
    this.spinner.hide();
    if (err) {
      if (err.status && err.status == 401) {
        this.errmsg = 'No Records.'
      } else {
      }
    } else {
    }
  }

  onCheckChange(event, category) {
    if (event && category) {
      if (event.target.checked){
        category.product.map(val => val.isChecked = true );
      } else {
        category.product.map(val => val.isChecked = false );
      }
    }
  }

}
