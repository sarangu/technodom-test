import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserViewService } from './user-view.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent implements OnInit {
  private userData: any;
  private sub: any;
  private errmsg: string;

  constructor(private route: ActivatedRoute, private uvs: UserViewService, private spinner: NgxSpinnerService) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       this.showUser(+params['id']); 
    });
  }

  showUser(id) {
    this.spinner.show();
this.uvs.showUser(id).pipe().subscribe(res => {
  this.success(res);
}, err => this.errhndlr(err));
  }

  success(data) {
    this.spinner.hide();
    if (data) {
      if (data.user) {
        this.userData = data.user
      } else {
        this.errmsg = 'No Records.'
      }
    } else {
      this.errmsg = 'No Records.'
    }
  }

  errhndlr(err) {
    this.spinner.hide();
    if (err) {
      if (err.status && err.status == 401) {
        this.errmsg = 'No Records.'
      } else {
      }
    } else {
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
