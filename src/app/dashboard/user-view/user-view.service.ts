import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserViewService {

  private apiURL = `${environment.apiUrl}`;
  private authHeader = {'Authorization': 'Bearer ' + localStorage.getItem('accessToken')};


  constructor(private httpClient: HttpClient) { }

  showUser(userId){
    return this.httpClient.get(this.apiURL + 'show_user/' + userId, {headers: this.authHeader});
  }
}
