import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiURL = `${environment.apiUrl}`;
  private authHeader = {'Authorization': 'Bearer ' + localStorage.getItem('accessToken')};


  constructor(private httpClient: HttpClient) { }

  allUsers(){
    return this.httpClient.get(this.apiURL + 'users', {headers: this.authHeader});
  }
}
