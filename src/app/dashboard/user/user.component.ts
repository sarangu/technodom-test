import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private router: Router, private us: UserService, private spinner: NgxSpinnerService) { }
  private errmsg: string;
  private usersList: [];

  ngOnInit() {
    this.getUsers();
  }

  getUserdata(id) {
    this.router.navigate(['/dashboard/users', id]);
  }

  getUsers() {
    this.spinner.show();
    this.us.allUsers().pipe().subscribe(res => {
      this.success(res);
    }, err => this.errhndlr(err));
  }

  success(data) {
    this.spinner.hide();
    if (data) {
      if (data.users) {
        this.usersList = data.users
      } else {
        this.errmsg = 'No Records.'
      }
    } else {
      this.errmsg = 'No Records.'
    }
  }

  errhndlr(err) {
    this.spinner.hide();
    if (err) {
      if (err.status && err.status == 401) {
        this.errmsg = 'No Records.'
      } else {
      }
    } else {
    }
  }

}
