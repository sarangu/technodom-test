import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { UserComponent } from './user/user.component';
import { ProductsComponent } from './products/products.component';
import { AuthGuard } from '../auth.guard';
import { UserViewComponent } from './user-view/user-view.component';


const routes: Routes = [{
  path: '',
  component: DashboardComponent,
  canActivateChild: [AuthGuard],
  children:[{
    path: '',
    redirectTo: 'users',
    pathMatch: 'full',
  },{
    path: 'users',
    component: UserComponent
  },{
    path: 'users/:id',
    component: UserViewComponent
  },{
    path: 'products',
    component: ProductsComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashbordRoutingModule { }
