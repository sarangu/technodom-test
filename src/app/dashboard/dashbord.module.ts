import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from "ngx-spinner";


import { DashbordRoutingModule } from './dashbord-routing.module';
import { UserComponent } from './user/user.component';
import { ProductsComponent } from './products/products.component';
import { DashboardComponent } from './dashboard.component';
import { SortByPipe } from '../shared/sort-by-pipe';
import { UserViewComponent } from './user-view/user-view.component';


@NgModule({
  declarations: [DashboardComponent, UserComponent, SortByPipe, ProductsComponent, UserViewComponent],
  imports: [
    CommonModule,
    NgxSpinnerModule,
    DashbordRoutingModule
  ]
})
export class DashbordModule { }
