import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

import { AuthService } from '../shared/auth.service';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private fb: FormBuilder, private loginService: LoginService, private spinner: NgxSpinnerService) { }
  loginForm: FormGroup;
  private errMsg: string;

  ngOnInit() {
    this.formValidation();
  }

  formValidation() {
    this.loginForm = this.fb.group({
      employee_id: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get formControls() { return this.loginForm.controls; }

  login(){
    // console.log(this.loginForm.value);
    if(this.loginForm.invalid){
      return;
    } else {
      this.spinner.show();
      this.loginService.signIn(this.loginForm.value).subscribe(res => {
        this.success(res)
      }, err => {
        this.errhndlr(err)
      });
    }
  }
  
  success(data) {
    this.spinner.hide();
    if (data) {
      if (data.token) {
          localStorage.setItem('accessToken', data.token);
          this.router.navigateByUrl('/dashboard');
      }
    } else {
      this.errMsg = 'Token Required'
    }
  }

  errhndlr(err) {
    this.spinner.hide();
    if (err) {
      if (err.status && err.status == 401) {
        this.errMsg = 'Invalid credentials';
      } else {
        this.errMsg = err.statusText;
      }
    } else {
      this.errMsg = 'Something went wrong.';
    }
  }

}
