import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  apiURL = `${environment.apiUrl}`;

  constructor(private httpClient: HttpClient) { }

  signIn(data: object){
    return this.httpClient.post(this.apiURL + 'login', data);
  }
}
