import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({ name: 'sortBy' })
export class SortByPipe implements PipeTransform {

    transform(array: Array<Object>, args: string): Array<Object> {
        // console.log(array, args);
        if (array == null) {
          return null;
        }
    
        array.sort((a,b) => a[args] > b[args] ? 1 : -1);
        return array;
      }
}